package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Activities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataMongoTest
@ExtendWith(SpringExtension.class)
public class ActivitiesDAOIntegrationTest {

    @Autowired
    private ActivitiesMongoDao activitiesMongoDao;

    @Test
    public void save_And_findById_Success() {
        LocalDateTime localDateTime = LocalDateTime.parse("2020-10-05T12:05:41.048");
        Activities activities = new Activities(localDateTime, "STRING", 1234, "STRING");

        Activities result = activitiesMongoDao.save(activities);

        assertEquals(activities.getAction(), result.getAction());
        assertEquals(activities.getClientId(), result.getClientId());
        assertEquals(activities.getNotes(), result.getNotes());
        assertEquals(activities.getTimeStamp(), result.getTimeStamp());
    }

    @Test
    public void findByClientIdAndTimeStampBetween_success() {
        LocalDateTime todayAt00 = LocalDate.now().atTime(0, 0, 0);
        LocalDateTime todayAt23 = LocalDate.now().atTime(23, 59, 59);
        LocalDateTime localDateTime = LocalDate.now().atTime(12, 05, 41);
        List<Activities> activitiesList = new ArrayList<>();
        activitiesList.add(new Activities(localDateTime, "Act1", 1234, "Note1"));
        activitiesList.add(new Activities(localDateTime, "Act2", 1234, "Note2"));
        activitiesList.add(new Activities(localDateTime, "Act3", 1234, "Note3"));


        List<Activities> result = activitiesMongoDao.saveAll(activitiesList);


        assertEquals(activitiesList, activitiesMongoDao.findByClientIdAndTimeStampBetween(1234, todayAt00, todayAt23));

    }

    @Test
    public void deleteActivityByClientId_success() {

        LocalDateTime localDateTime = LocalDate.now().atTime(12, 05, 41);
        Activities result = activitiesMongoDao.save(new Activities(localDateTime, "Act3", 1234, "Note3"));

        activitiesMongoDao.deleteByClientId(1234);


        assertTrue(CollectionUtils.isEmpty(activitiesMongoDao.findByClientId(1234)));
    }
}
