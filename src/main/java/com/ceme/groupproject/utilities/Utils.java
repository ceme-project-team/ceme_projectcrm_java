package com.ceme.groupproject.utilities;

import com.ceme.groupproject.entities.Customer;

public class Utils {

    Utils() {
        //Utils Class
    }

    //validation method to test object is populated with data.
    public static boolean validateCustomer(Customer customer) {
        return customer.getId() > 0
                && !customer.getFirstName().isEmpty()
                && !customer.getLastName().isEmpty()
                && !customer.getAddressLine().isEmpty()
                && !customer.getCity().isEmpty()
                && !customer.getState().isEmpty()
                && customer.getZip() > 0
                && customer.getDob() != null;
    }

}
