package com.ceme.groupproject.entities;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ActivitiesTest {

    private Activities activities;
    private LocalDateTime localDateTime;

    @BeforeEach
    void setUp() {
        localDateTime = LocalDateTime.now();
        activities = new Activities(localDateTime, "STRING", 1234, "STRING");
    }

    @Test
    void getTimestamp() {
        assertEquals(localDateTime, activities.getTimeStamp());
    }

    @Test
    void getAction() {
        assertEquals("STRING", activities.getAction());
    }

    @Test
    void getClientId() {
        assertEquals(1234, activities.getClientId());
    }

    @Test
    void getNotes() {
        assertEquals("STRING", activities.getNotes());
    }

    @Test
    void setId() {
        ObjectId objectId = new ObjectId();
        activities.setId(objectId);
        assertEquals(objectId, activities.getId());
    }

    @Test
    void setTimestamp() {
        LocalDateTime localDateTime1 = LocalDateTime.now();
        activities.setTimeStamp(localDateTime1);
        assertEquals(localDateTime1, activities.getTimeStamp());
    }

    @Test
    void setAction() {
        activities.setAction("String");
        assertEquals("String", activities.getAction());
    }

    @Test
    void setClientId() {
        activities.setClientId(999);
        assertEquals(999, activities.getClientId());
    }

    @Test
    void setNotes() {
        activities.setNotes("string");
        assertEquals("string", activities.getNotes());
    }

}