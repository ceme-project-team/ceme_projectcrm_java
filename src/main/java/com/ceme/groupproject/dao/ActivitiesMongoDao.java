package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Activities;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ActivitiesMongoDao extends MongoRepository<Activities, ObjectId> {
    List<Activities> findByClientId(int id);
    List<Activities> findByClientIdAndTimeStampBetween(Integer clientId, LocalDateTime startDate, LocalDateTime endDate);
    void deleteByClientId(int custId);
}
