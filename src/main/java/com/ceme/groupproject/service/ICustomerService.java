package com.ceme.groupproject.service;

import com.ceme.groupproject.entities.Customer;
import com.ceme.groupproject.exception.CustomerException;

import java.util.List;

public interface ICustomerService {
    boolean save(Customer customer) throws CustomerException;
    Customer findById(int id);
    List<Customer> findAll() throws CustomerException;
    String deleteCustomerById(int id) throws CustomerException;
    String updateCustomer(Customer customer) throws CustomerException;
    int generateId();
}
