package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
@ExtendWith(SpringExtension.class)
public class CustomerDAOIntegrationTest {

    @Autowired
    private CustomerMongoDao customerMongoDao;

    @Test
    public void save_And_findById_Success() {
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                localDate, "text");

        customerMongoDao.save(customer);

        assertEquals(customer, customerMongoDao.findById(1));
    }

    @Test
    public void save_AndFindAll_Success() {
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(2, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                localDate, "text");

        customerMongoDao.save(customer);
        List<Customer> paymentList = customerMongoDao.findAll();
        assertNotEquals(0, paymentList.size());
    }

    @Test
    public void delete_customer() {
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(22, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                localDate, "text");
        customerMongoDao.save(customer);

        customerMongoDao.deleteCustomerById(22);

        assertNull(customerMongoDao.findById(22));

    }

    @Test
    public void update_And_findById_Success() {
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(2, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                localDate, "text");
        customerMongoDao.save(customer);
        customer.setFirstName("updatedfirstname");
        customerMongoDao.save(customer);

        assertEquals(customer, customerMongoDao.findById(2));
    }
}
