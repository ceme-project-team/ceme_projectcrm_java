#!/usr/bin/env groovy
pipeline{
    agent any

    stages{
        stage('BUILD'){
            steps{
                sh './gradlew clean build'
            }
        }
        stage('DELIVERY'){
            steps{
                fileOperations([
                        fileCopyOperation(
                                excludes: '',
                                flattenFiles: false,
                                includes: 'build\\libs\\**',
                                targetLocation: 'C:\\deployments\\java'
                        )
                ])
            }
        }
    }
}