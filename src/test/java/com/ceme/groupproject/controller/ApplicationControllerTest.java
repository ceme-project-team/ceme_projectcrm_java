package com.ceme.groupproject.controller;

import com.ceme.groupproject.entities.Activities;
import com.ceme.groupproject.entities.Customer;
import com.ceme.groupproject.service.ApplicationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

class ApplicationControllerTest {


    private MockMvc mockMvc;

    @Mock
    private ApplicationService applicationService;

    private ApplicationController applicationController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        applicationController = new ApplicationController(applicationService);
        mockMvc = standaloneSetup(applicationController).build();
    }

    @Test
    void postCustomer_success() throws Exception {
        doReturn(true).when(applicationService).save(any(Customer.class));

        mockMvc.perform(MockMvcRequestBuilders.post("/postCustomer")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Customer Added!"));

        verify(applicationService).save(any(Customer.class));
    }

    @Test
    void findAll_success() throws Exception {
        List<Customer> paymentList = new ArrayList<Customer>();
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(2, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                localDate, "text");
        paymentList.add(customer);
        Customer customer2 = new Customer(2, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                localDate, "text");
        paymentList.add(customer2);
        doReturn(paymentList).when(applicationService).findAll();

        mockMvc.perform(MockMvcRequestBuilders.get("/all")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());

        verify(applicationService).findAll();
    }

    @Test
    void updateCustomer_success() throws Exception {
        doReturn(true).when(applicationService).save(any(Customer.class));

        mockMvc.perform(MockMvcRequestBuilders.post("/postUpdate")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Customer Updated!"));

        verify(applicationService).updateCustomer(any(Customer.class));
    }

    @Test
    void deleteActivityByClientId_success() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/deleteActivityByClientId/1234"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("deleted!"));
        verify(applicationService).deleteActivityByClientId(any(Integer.class));
    }

    @Test
    void getActivityByDateRange_success() throws Exception {
        doReturn(Collections.singletonList(new Activities())).when(applicationService)
                .findActivityByDateRange(any(Integer.class), any(LocalDate.class), any(LocalDate.class));

        mockMvc.perform(MockMvcRequestBuilders.get("/getActivityByDateRange")
                .param("clientId", String.valueOf(1))
                .param("fromDate", LocalDate.now().toString())
                .param("toDate", LocalDate.now().toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$..id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$..timeStamp").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$..action").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$..clientId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$..notes").exists())
        ;

        verify(applicationService).findActivityByDateRange(any(Integer.class), any(LocalDate.class), any(LocalDate.class));
    }

}