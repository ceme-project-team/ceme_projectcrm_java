package com.ceme.groupproject.exception;

public class ActivitiesException extends RuntimeException {
    public ActivitiesException(String message) {
        super(message);
    }

}
