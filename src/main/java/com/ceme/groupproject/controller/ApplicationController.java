package com.ceme.groupproject.controller;

import com.ceme.groupproject.entities.Activities;
import com.ceme.groupproject.entities.Customer;
import com.ceme.groupproject.exception.CustomerException;
import com.ceme.groupproject.service.ApplicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin(value = "*", methods = {DELETE, POST, GET}) //Need this for the REACT App
@RestController
public class ApplicationController {

    private final ApplicationService applicationService;
    private final Logger logger;

    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
        logger = Logger.getLogger(ApplicationService.class.getName());
    }

    @PostMapping(value = "/postCustomer")
    String postCustomer(@RequestBody Customer customer) {
        applicationService.save(customer);
        return "Customer Added!"; //Stubbed return
    }

    @PostMapping(value = "/postActivity")
    String postInteraction(@RequestBody Activities activities) {
        applicationService.save(activities);
        return "Activity Added!"; //Stubbed return
    }

    @DeleteMapping(value = "/deleteCustomer/{id}")
    String deleteCustomer(@PathVariable ("id") int id) {
        applicationService.deleteCustomerById(id);
        return "Deleted Customer!"; //Stubbed return
    }

    @RequestMapping(value = "/all", method = GET)
    public List<Customer> findAll() {
        return applicationService.findAll();
    }

    @DeleteMapping(value = "/deleteActivityByClientId/{clientId}")
    public String deleteActivityByClientId(@PathVariable("clientId") int clientId) {
        applicationService.deleteActivityByClientId(clientId);
        return "deleted!";
    }

    @GetMapping(value = "/getActivityByDateRange")
    public List<Activities> getActivityByDateRange(@RequestParam String clientId,
                                                   @RequestParam String fromDate,
                                                   @RequestParam String toDate) {

        return applicationService.findActivityByDateRange(Integer.parseInt(clientId), LocalDate.parse(fromDate), LocalDate.parse(toDate));
    }



    @RequestMapping(value = "/status", method = GET)
    public String status() {
        logger.info("The API is running");
        return "Rest Api is running!";}

    @RequestMapping(value = "/findById/{id}", method = GET)
    public Customer findById(@PathVariable("id") int id) {
        logger.info("Finding by ID");
        return applicationService.findById(id);
    }

    @RequestMapping(value = "/findByClientId/{clientId}", method = GET)
    public List<Activities> findByClientId(@PathVariable("clientId") int clientId) {
        return applicationService.findByClientId(clientId);
    }

    @RequestMapping(value = "/postUpdate", method = POST)
    String postUpdate(@RequestBody Customer customer) {
        applicationService.updateCustomer(customer);
        return "Customer Updated!"; //Stubbed return
    }

    @RequestMapping(value = "/postUpdateActivity", method = POST)
    String postUpdateActivity(@RequestBody Activities activities) {
        applicationService.save(activities);
        return "Activity Updated!"; //Stubbed return
    }

    @GetMapping(value = "/generateId")
    public int generateId(){
        int newId = applicationService.generateId();
        logger.info("New ID Generated: " + newId);
       return newId;
    }


    //Controller Advice
    @ExceptionHandler({CustomerException.class})
    public ResponseEntity<String> handleException(CustomerException e) {
        logger.warning(String.format("Exception Thrown: %s", e.getMessage()));
        return ResponseEntity.badRequest().body(e.getMessage());
    }

}
