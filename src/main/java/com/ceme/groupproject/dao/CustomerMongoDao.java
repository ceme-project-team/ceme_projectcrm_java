package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerMongoDao extends MongoRepository<Customer, Integer> {
    Customer findById(int id);
    List<Customer> findAll();
    void deleteCustomerById(int id);
    Customer findFirstByOrderByIdDesc();
}
