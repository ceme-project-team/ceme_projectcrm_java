package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Activities;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ActivitiesDAOTest {

    @Mock
    private ActivitiesMongoDao activitiesMongoDao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(activitiesMongoDao).count();
        assertEquals(1L, activitiesMongoDao.count());
    }

    @Test
    void save_success() {
        LocalDateTime localDateTime = LocalDateTime.now();
        doReturn(new Activities(localDateTime, "STRING", 1234, "STRING"))
                .when(activitiesMongoDao)
                .save(any(Activities.class));

        Activities activities = new Activities(localDateTime, "STRING", 1234, "STRING");

        assertEquals(activities, activitiesMongoDao.save(new Activities(localDateTime, "STRING", 1234, "STRING")));

    }

    @Test
    void findByClientIdAndTimeStampBetween_success() {
        LocalDateTime todayAt00 = LocalDate.now().atTime(0, 0, 0);
        LocalDateTime todayAt23 = LocalDate.now().atTime(23, 59, 59);
        LocalDateTime localDateTime = LocalDateTime.now();
        List<Activities> activitiesList = new ArrayList<>();
        activitiesList.add(new Activities(localDateTime, "Act1", 1234, "Note1"));
        activitiesList.add(new Activities(localDateTime, "Act2", 1234, "Note2"));
        activitiesList.add(new Activities(localDateTime, "Act3", 1234, "Note3"));


        doReturn(activitiesList)
                .when(activitiesMongoDao)
                .findByClientIdAndTimeStampBetween(any(Integer.class), any(LocalDateTime.class), any(LocalDateTime.class));

        assertEquals(activitiesList, activitiesMongoDao.findByClientIdAndTimeStampBetween(1234, todayAt00, todayAt23));

    }

    @Test
    void deleteActivity_success() {
        int custID = 1;
        activitiesMongoDao.deleteByClientId(custID);
        verify(activitiesMongoDao, times(1)).deleteByClientId(eq(custID));
    }
}