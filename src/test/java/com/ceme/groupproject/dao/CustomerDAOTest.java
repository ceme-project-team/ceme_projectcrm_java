package com.ceme.groupproject.dao;

import com.ceme.groupproject.entities.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class CustomerDAOTest {

    @Mock
    private CustomerMongoDao customerMongoDao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(customerMongoDao).count();
        assertEquals(1L, customerMongoDao.count());
    }

    @Test
    void save_success() {
        LocalDate date = LocalDate.now();
        doReturn(new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                date, "text"))
                .when(customerMongoDao)
                .save(any(Customer.class));

        Customer customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                date, "text");

        assertEquals(customer, customerMongoDao.save(new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                date, "text")));

    }

    @Test
    public void List_findAll_success() {
        List<Customer> paymentList = new ArrayList<Customer>();
        LocalDate date = LocalDate.now();
        Customer customer = new Customer(3, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                date, "text");
        paymentList.add(customer);
        Customer customer2 = new Customer(4, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                date, "text");
        paymentList.add(customer2);
        doReturn(paymentList)
                .when(customerMongoDao)
                .findAll();
        assertNotEquals(0, customerMongoDao.findAll().size());
    }

    @Test
    void deleteCustomer(){
        int custID = 1;
        customerMongoDao.deleteCustomerById(custID);
        verify(customerMongoDao, times(1)).deleteCustomerById(eq(custID));
    }

    //    @Test
//    void update_success() {
//        LocalDate date = LocalDate.now();
//        Customer customer = new Customer(6, "prefix", "firstname", "middlename", "lastname", "suffix",
//                "address", "city", "state", 1, 2, "email",
//                date, "text");
//        customerMongoDao.save(customer);
//
//        Customer customerUpd = new Customer(6, "prefix", "updatedfirstname", "middlename", "lastname", "suffix",
//                "address", "city", "state", 1, 2, "email",
//                date, "text");
//        customerMongoDao.save(customerUpd);
//        Customer customerChk = new Customer();
//        customerChk = customerMongoDao.findById(6);
//        assertEquals("updatedfirstname", customerChk.getFirstName());
//    }
}