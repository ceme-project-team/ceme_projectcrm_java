package com.ceme.groupproject.service;

import com.ceme.groupproject.dao.ActivitiesMongoDao;
import com.ceme.groupproject.dao.CustomerMongoDao;
import com.ceme.groupproject.entities.Activities;
import com.ceme.groupproject.entities.Customer;
import com.ceme.groupproject.exception.CustomerException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ApplicationServiceTest {


    @Mock
    private CustomerMongoDao customerMongoDaoMock;

    @Mock
    private ActivitiesMongoDao activitiesMongoDaoMock;

    @InjectMocks
    private ApplicationService applicationService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        applicationService = new ApplicationService(activitiesMongoDaoMock, customerMongoDaoMock);
    }

    @Test
    void deleteActivity_success() {
        applicationService.deleteActivityByClientId(1234);
        verify(activitiesMongoDaoMock, times(1)).deleteByClientId(anyInt());
    }

    @Test
    void findActivityByDateRange_success() {
        LocalDate today = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.now();
        List<Activities> activitiesList = new ArrayList<>();
        activitiesList.add(new Activities(localDateTime, "Act1", 1234, "Note1"));
        activitiesList.add(new Activities(localDateTime, "Act2", 1234, "Note2"));
        activitiesList.add(new Activities(localDateTime, "Act3", 1234, "Note3"));


        doReturn(activitiesList)
                .when(activitiesMongoDaoMock)
                .findByClientIdAndTimeStampBetween(any(Integer.class), any(LocalDateTime.class), any(LocalDateTime.class));

        assertEquals(activitiesList, applicationService.findActivityByDateRange(1234, today, today));
    }

    @Test
    void saveCustomerAndActivity_success() {
        LocalDateTime localDateTime = LocalDateTime.now();
        Activities activities = new Activities(localDateTime, "STRING", 1234, "STRING");

        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 12341, 2, "email",
                localDate, "text");

        doReturn(activities).when(activitiesMongoDaoMock).save(any(Activities.class));
        doReturn(customer).when(customerMongoDaoMock).save(any(Customer.class));

        boolean result = applicationService.save(customer);

        assertEquals(true, result);
    }

    @Test
    void deleteCustomer() {
        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 12341, 2, "email",
                localDate, "text");

        String result  = applicationService.deleteCustomerById(customer.getId());

        assertEquals("DELETED", result);
    }


    @Test
    void saveCustomerAndActivity_CustomerException() {
        Throwable exception =
                assertThrows(CustomerException.class, () -> {
                    applicationService.save(new Customer());
                });
        assertEquals("Invalid inputs!", exception.getMessage());
    }

    @Test
    void findAll_success() {
        List<Customer> paymentList = new ArrayList<Customer>();
        LocalDate date = LocalDate.now();
        Customer customer = new Customer(3, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                date, "text");
        paymentList.add(customer);
        Customer customer2 = new Customer(4, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 2, 4, "email",
                date, "text");
        paymentList.add(customer2);
        doReturn(paymentList)
                .when(customerMongoDaoMock)
                .findAll();
        assertNotEquals(0, applicationService.findAll().size());
    }

    @Test
    void updateCustomer_CustomerException() {
        Throwable exception =
                assertThrows(CustomerException.class, () -> {
                    applicationService.updateCustomer(new Customer());
                });
        assertEquals("Invalid inputs!", exception.getMessage());
    }

    @Test
    void updateCustomer_success() {

        LocalDate localDate = LocalDate.now();
        Customer customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 12341, 2, "email",
                localDate, "text");

        doReturn(customer).when(customerMongoDaoMock).save(any(Customer.class));

        String result = applicationService.updateCustomer(customer);

        assertEquals("UPDATED", result);
    }

}