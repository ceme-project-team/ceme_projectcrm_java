package com.ceme.groupproject.service;

import com.ceme.groupproject.entities.Activities;
import com.ceme.groupproject.exception.ActivitiesException;

import java.time.LocalDate;
import java.util.List;

public interface IActivitiesService {
    void save(Activities activities) throws ActivitiesException;
    List<Activities> findByClientId(int clientId);
    List<Activities> findActivityByDateRange(int clientId, LocalDate from, LocalDate to);
    void deleteActivityByClientId(int clientId);
    void updateActivity(Activities activities) throws ActivitiesException;
}
