package com.ceme.groupproject.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Data
@NoArgsConstructor
//@AllArgsConstructor
public class Activities {

    @Id
    private ObjectId id;
    private LocalDateTime timeStamp;
    private String action;
    private int clientId;
    private String notes;

    public Activities(LocalDateTime timeStamp, String action, int clientId, String notes) {
        this.timeStamp = timeStamp;
        this.action = action;
        this.clientId = clientId;
        this.notes = notes;
    }
}
