package com.ceme.groupproject.service;

import com.ceme.groupproject.dao.ActivitiesMongoDao;
import com.ceme.groupproject.dao.CustomerMongoDao;
import com.ceme.groupproject.entities.Activities;
import com.ceme.groupproject.entities.Customer;
import com.ceme.groupproject.exception.CustomerException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.ceme.groupproject.utilities.Utils.validateCustomer;

@Service
public class ApplicationService implements IActivitiesService, ICustomerService {

    private final ActivitiesMongoDao activitiesMongoDao;
    private final CustomerMongoDao customerMongoDao;
    private final Logger logger;

    public ApplicationService(ActivitiesMongoDao activitiesMongoDao, CustomerMongoDao customerMongoDao) {
        this.activitiesMongoDao = activitiesMongoDao;
        this.customerMongoDao = customerMongoDao;
        logger = Logger.getLogger(ApplicationService.class.getName());
    }

    private Activities createNewCustomerActivity(Customer customer, String action, String notes) {
        return new Activities(LocalDateTime.now(), action, customer.getId(), notes);
    }

    private Activities createNewCustomerActivity(int clientId, String action, String notes) {
        return new Activities(LocalDateTime.now(), action, clientId, notes);
    }
    @Override
    public void save(Activities activities) {
        activitiesMongoDao.save(activities);
    }

    @Override
    public List<Activities> findActivityByDateRange(int clientId, LocalDate from, LocalDate to) {
        return activitiesMongoDao.findByClientIdAndTimeStampBetween(clientId, from.atStartOfDay(), to.atTime(LocalTime.MAX));
    }

    @Override
    public void deleteActivityByClientId(int clientId) {
        activitiesMongoDao.deleteByClientId(clientId);
    }

    @Override
    public boolean save(Customer customer) {
        if (validateCustomer(customer)) {
            customerMongoDao.save(customer);
            save(createNewCustomerActivity(customer, "ADD CUSTOMER", ""));
            logger.info("created new customer and activity!");
        } else {
            throw new CustomerException("Invalid inputs!");
        }
        return true;
    }

    @Override
    public List<Customer> findAll() {
        return new ArrayList<>(customerMongoDao.findAll());
    }

    @Override
    public String deleteCustomerById(int id) {
            customerMongoDao.deleteCustomerById(id);
            save(createNewCustomerActivity(id, "DELETE CUSTOMER", ""));
            logger.info("deleted customer and added new delete customer activity");
             return "DELETED";
    }

    @Override
    public String updateCustomer(Customer customer) {
        if (validateCustomer(customer)) {
            customerMongoDao.save(customer);
            save(createNewCustomerActivity(customer, "UPDATE CUSTOMER", ""));
            logger.info("updated customer and activity!");
        } else {
            throw new CustomerException("Invalid inputs!");
        }
        return "UPDATED";
    }

    @Override
    public int generateId() {
        int newId = customerMongoDao.findFirstByOrderByIdDesc().getId();
        if(newId>0){
            return newId+1;
        } else {
            return 101;
        }
    }

    @Override
    public Customer findById(int id) {
        if (id > 0) {
            return customerMongoDao.findById(id);
        } else {
            return null;
        }
    }

    @Override
    public List<Activities> findByClientId(int clientId) {
        if (clientId > 0) {
            return activitiesMongoDao.findByClientId(clientId);
        } else {
            return null;
        }
    }

    @Override
    public void updateActivity(Activities activities) {
        activitiesMongoDao.save(activities);
    }
}

