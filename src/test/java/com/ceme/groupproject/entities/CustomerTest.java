package com.ceme.groupproject.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTest {

    private Customer customer;
    private LocalDate localDate;

    @BeforeEach
    void setUp() {
        localDate = LocalDate.now();
        customer = new Customer(1, "prefix", "firstname", "middlename", "lastname", "suffix", "gender",
                "address", "city", "state", 1, 2, "email",
                localDate, "text");
    }

    @Test
    void getId() {
        assertEquals(1, customer.getId());
    }

    @Test
    void getPrefix() {
        assertEquals("prefix", customer.getPrefix());
    }

    @Test
    void getFirstname() {
        assertEquals("firstname", customer.getFirstName());
    }

    @Test
    void getMiddlename() {
        assertEquals("middlename", customer.getMiddleName());
    }

    @Test
    void getLastname() {
        assertEquals("lastname", customer.getLastName());
    }

    @Test
    void getSuffix() {
        assertEquals("suffix", customer.getSuffix());
    }

    @Test
    void getGender() {
        assertEquals("gender", customer.getGender());
    }

    @Test
    void getAddressLine() {
        assertEquals("address", customer.getAddressLine());
    }

    @Test
    void getCity() {
        assertEquals("city", customer.getCity());
    }

    @Test
    void getState() {
        assertEquals("state", customer.getState());
    }

    @Test
    void getZip() {
        assertEquals(1, customer.getZip());
    }

    @Test
    void getPhonenumber() {
        assertEquals(2, customer.getPhoneNumber());
    }

    @Test
    void getEmailAddress() {
        assertEquals("email", customer.getEmailAddress());
    }

    @Test
    void getDob() {
        assertEquals(localDate, customer.getDob());
    }

    @Test
    void getAdditionalInfo() {
        assertEquals("text", customer.getAdditionalInfo());
    }

    @Test
    void setId() {
        customer.setId(999);
        assertEquals(999, customer.getId());
    }

    @Test
    void setPrefix() {
        customer.setPrefix("prefix");
        assertEquals("prefix", customer.getPrefix());
    }

    @Test
    void setFirstname() {
        customer.setFirstName("firstname");
        assertEquals("firstname", customer.getFirstName());
    }

    @Test
    void setMiddlename() {
        customer.setMiddleName("middlename");
        assertEquals("middlename", customer.getMiddleName());
    }

    @Test
    void setLastname() {
        customer.setLastName("lastname");
        assertEquals("lastname", customer.getLastName());
    }

    @Test
    void setSuffix() {
        customer.setSuffix("suffix");
        assertEquals("suffix", customer.getSuffix());
    }

    @Test
    void setAddressLine() {
        customer.setAddressLine("address");
        assertEquals("address", customer.getAddressLine());
    }

    @Test
    void setCity() {
        customer.setCity("city");
        assertEquals("city", customer.getCity());
    }

    @Test
    void setState() {
        customer.setState("state");
        assertEquals("state", customer.getState());
    }

    @Test
    void setZip() {
        customer.setZip(1);
        assertEquals(1, customer.getZip());
    }

    @Test
    void setPhonenumber() {
        customer.setPhoneNumber(2);
        assertEquals(2, customer.getPhoneNumber());
    }

    @Test
    void setEmailAddress() {
        customer.setEmailAddress("email");
        assertEquals("email", customer.getEmailAddress());
    }

    @Test
    void setDob() {
        customer.setDob(localDate);
        assertEquals(localDate, customer.getDob());
    }

    @Test
    void setAdditionalInfo() {
        customer.setAdditionalInfo("text");
        assertEquals("text", customer.getAdditionalInfo());
    }


}